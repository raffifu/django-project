FROM python:3.11-slim

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN python3 django_project/manage.py collectstatic

EXPOSE 8000

CMD [ "./scripts/entrypoint.sh" ]