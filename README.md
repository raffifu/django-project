# Sample Django Project
Sample Django project that implement CI/CD for deployment

## Endpoint
- `/users/` -> Show user data from database
- `/clock/` -> Show server time

## How to build and run
1. Build the image with `docker build -t django-project .`
2. Run `docker run -e 'DJANGO_ENV=development' django-project`

## Note
1. Set `DJANGO_ENV` to `production` if you want to deploy on production
2. Prepare the database and input the credential as env var (see `env.example` on `django_project` folder)