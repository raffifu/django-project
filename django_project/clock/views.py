from django.shortcuts import render
from datetime import datetime


# Create your views here.
def index(request):
    context = {"current_time": str(datetime.now())}
    return render(request, "clock/index.html", context)
