from django.db import models


# Create your models here.
class User(models.Model):
    fullname = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    age = models.IntegerField()
    address = models.TextField()
