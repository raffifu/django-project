from django.test import TestCase
from user.models import User


# Create your tests here.
class UserModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        User.objects.create(
            fullname="John Doe",
            email="john.doe@email.com",
            age=35,
            address="Street number ..",
        )

    def test_fullname_max_length(self):
        user = User.objects.get(id=1)

        max_length = user._meta.get_field("fullname").max_length
        self.assertEqual(max_length, 256)

    def test_fullname_max_length(self):
        user = User.objects.get(id=1)

        max_length = user._meta.get_field("email").max_length
        self.assertEqual(max_length, 256)
