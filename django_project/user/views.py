from django.shortcuts import render
from django.views.generic import ListView
from .models import User


class UserListView(ListView):
    model = User
    context_object_name = "user_list"
    template_name = "user/index.html"
