#!/bin/sh

cd django_project

python3 manage.py migrate
gunicorn -b 0.0.0.0:8000 django_project.wsgi